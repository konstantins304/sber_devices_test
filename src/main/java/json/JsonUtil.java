package json;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Stack;

public class JsonUtil {

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static String fieldNamesToCamelCase(String json) {
        JsonNode rootNode = readTree(json);

        Stack<JsonNode> stack = new Stack<>();
        stack.push(rootNode);

        JsonNode nextNode;
        while (!stack.isEmpty()) {
            nextNode = stack.pop();
            if (nextNode.isContainerNode()) {
                if (nextNode.isObject()) {
                    Iterator<Map.Entry<String, JsonNode>> fieldsIterator = nextNode.fields();
                    Map<String, JsonNode> nodes = new LinkedHashMap<>();
                    while (fieldsIterator.hasNext()) {
                        Map.Entry<String, JsonNode> field = fieldsIterator.next();
                        JsonNode innerNode = field.getValue();
                        if (innerNode.isContainerNode()) {
                            stack.push(innerNode);
                        }
                        nodes.put(toCamelCase(field.getKey()), innerNode);
                        fieldsIterator.remove();
                    }
                    ((ObjectNode)nextNode).setAll(nodes);
                } else if (nextNode.isArray()) {
                    for (JsonNode jsonNode : nextNode) {
                        stack.push(jsonNode);
                    }
                }
            }
        }

        return rootNode.toPrettyString();
    }

    private static String toCamelCase(String fieldName) {
        if (fieldName.contains("_")) {
            return fromSnackCase(fieldName);
        }

        return fromSnackCase(fromPascalCase(fieldName));
    }

    private static String fromSnackCase(String fieldName) {
        String[] strings = fieldName.split("_");
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(strings[0].toLowerCase());
        for (int i = 1; i < strings.length; i++) {
            String s = strings[i];
            stringBuilder.append(s.substring(0, 1).toUpperCase())
                    .append(s.substring(1).toLowerCase());
        }

        return stringBuilder.toString();
    }

    private static String fromPascalCase(String fieldName) {
        String regex = "([a-z])([A-Z]+)";
        String replacement = "$1_$2";

        return fieldName
                .replaceAll(regex, replacement)
                .toLowerCase();
    }

    private static JsonNode readTree(String json) {
        try {
            return OBJECT_MAPPER.readTree(json);
        } catch (IOException ex) {
            throw new RuntimeException("Error during parse incoming json!", ex);
        }
    }
}
