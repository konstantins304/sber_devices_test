package json;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

public class ResourceUtil {

    public String readResource(String resource) {
        InputStream resourceAsStream = getClass().getClassLoader().getResourceAsStream(resource);
        if (resourceAsStream == null) {
            throw new IllegalArgumentException("Resource doesn't exists!");
        }

        try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream))) {
            return bufferedReader.lines().collect(Collectors.joining());
        } catch (IOException ex) {
            throw new RuntimeException("Error during read resource = " + resource, ex);
        }
    }
}
