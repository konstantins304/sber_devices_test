package json;


import static org.skyscreamer.jsonassert.JSONAssert.assertEquals;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.skyscreamer.jsonassert.JSONCompareMode;

public class JsonUtilTest {

    private final ResourceUtil resourceUtil = new ResourceUtil();

    private static final String TEST_INCOMING_JSON_PATH = "testIncomingJson.json";
    private static final String TEST_RESULT_JSON_PATH = "testIncomingJsonResult.json";

    @Test
    void testFieldNamesToCamelCase() throws JSONException {
        final String incomingJson = resourceUtil.readResource(TEST_INCOMING_JSON_PATH);

        assertEquals(
                resourceUtil.readResource(TEST_RESULT_JSON_PATH),
                JsonUtil.fieldNamesToCamelCase(incomingJson),
                JSONCompareMode.LENIENT
        );
    }
}
